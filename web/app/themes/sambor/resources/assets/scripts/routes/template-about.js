import jump from 'jump.js'

export default {
  init() {
    const scrollButtons = document.querySelectorAll('.scroll-down, .scroll-up');
    if (scrollButtons) {
      scrollButtons.forEach((scrollButton) => {
        scrollButton.addEventListener('click', () => {
          jump(scrollButton.getAttribute('data-scrollTo'));
        });
      });
    }
  },
};
