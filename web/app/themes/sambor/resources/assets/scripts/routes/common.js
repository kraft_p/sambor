import jump from 'jump.js';
import Hammer from 'hammerjs';
import LazyLoad from 'vanilla-lazyload';

export default {
  init() {
    // JavaScript to be fired on all pages
    const isMobile = window.innerWidth < 768;

    const menu = document.querySelector('.nav-primary');
    const menuButton = document.querySelector('.hamburger');

    menuButton.addEventListener('click', () => {
      menuButton.classList.toggle('hamburger--active');
      menu.classList.toggle('nav-primary--show');
      document.body.classList.toggle('menu-active');
    });

    const grid = document.querySelector('.grid');
    if (grid) {
      const scrollDownButtons = document.querySelectorAll('.scroll-down');
      scrollDownButtons.forEach((scrollDownButton) => {
        scrollDownButton.addEventListener('click', () => {
          jump(scrollDownButton.getBoundingClientRect().top + 25);
        }); 
      });

      const gridItems = grid.querySelectorAll('.grid__item:not(.grid__item--empty):not(.grid__item--category)');
      const gridItemsCount = gridItems.length;
      const emptyGridItems = grid.querySelectorAll('.grid__item--empty');

      const updateEmptyGridItems = () => {
        const items = grid.querySelectorAll('.grid__item:not(.grid__item--empty)');
        const itemsCount = items.length;

        const windowWidth = window.innerWidth;

        let itemsInRow;
        
        if (windowWidth >= 1600) {
          itemsInRow = 5;
        } else if (windowWidth >= 1024) {
          itemsInRow = 4;
        } else if (windowWidth >= 768) {
          itemsInRow = 3;
        } else {
          itemsInRow = 2;
        }

        const emtyGridItemsToShow = itemsInRow - (itemsCount % itemsInRow);

        for (let i = 0; i < emptyGridItems.length; i++ ) {
          if (emtyGridItemsToShow - i> 0 && emtyGridItemsToShow < itemsInRow) {
            emptyGridItems[i].style.display = 'block';
          } else {
            emptyGridItems[i].style.display = '';
          }
        }
      }

      const updateScrollButtons = () => {
        const windowWidth = window.innerWidth;

        if ((windowWidth >= 1600 && gridItemsCount <= 9) || (windowWidth >= 1024 && gridItemsCount <= 7)) {
          scrollDownButtons.forEach((scrollDownButton) => {
            scrollDownButton.style.display = 'none';
          });
        } else {
          scrollDownButtons.forEach((scrollDownButton) => {
            scrollDownButton.style.display = '';
          });
        }
      };

      const windowResizeHandler = () => {
        updateEmptyGridItems();
        updateScrollButtons();
      };

      window.addEventListener('resize', windowResizeHandler);
      windowResizeHandler();

      const lightbox = document.querySelector('.lightbox');
      const lightboxImage = lightbox.querySelector('.lightbox__image');
      const lightboxPrevButton = lightbox.querySelector('.lightbox__button--prev');
      const lightboxNextButton = lightbox.querySelector('.lightbox__button--next');
      const overlay = document.querySelector('.lightbox-overlay');
      let currentLightboxItem;

      const hideLightbox = () => {
        document.body.classList.remove('overlay');
        overlay.style.display = '';
        lightbox.style.display = '';
        lightbox.querySelector('.lightbox__image').src = '';
      };

      const changeLightboxImage = (src, callback) => {
        if (callback) {
          lightboxImage.addEventListener('load', callback);
        }

        lightboxImage.src = src;
      };

      const getLightboxImageSrc = (node) => {
        if (isMobile) {
          return node.src;
        } else {
          return node.getAttribute('data-lightboxsrc');
        }
      };
      
      const showLightbox = (item) => {
        const callback = () => {
          document.body.classList.add('overlay');
          overlay.style.display = 'block';
          lightbox.style.display = 'block';
          lightboxImage.removeEventListener('load', callback);
        };

        changeLightboxImage(getLightboxImageSrc(item.querySelector('.thumbnail')), callback);
      };

      const showNextSlide = () => {
        const nextItem = currentLightboxItem.nextElementSibling;
        if (nextItem && !nextItem.classList.contains('grid__item--empty') && !nextItem.classList.contains('grid__item--category')) {
          currentLightboxItem = nextItem;
        } else {
          currentLightboxItem = gridItems[0];
        }

        changeLightboxImage(getLightboxImageSrc(currentLightboxItem.querySelector('.thumbnail')));
      };

      const showPrevSlide = () => {
        const prevItem = currentLightboxItem.previousElementSibling;
        if (prevItem && !prevItem.classList.contains('grid__item--empty') && !prevItem.classList.contains('grid__item--category')) {
          currentLightboxItem = prevItem;
        } else {
          currentLightboxItem = gridItems[gridItemsCount - 1];
        }

        changeLightboxImage(getLightboxImageSrc(currentLightboxItem.querySelector('.thumbnail')));
      };

      for (let i = 0; i < gridItemsCount; i++) {
        const gridItem = gridItems[i];
        gridItem.querySelector('.title').addEventListener('click', () => {
          showLightbox(gridItem);
          currentLightboxItem = gridItem;
        });
      }

      overlay.addEventListener('click', hideLightbox);
      lightbox.querySelector('.lightbox__button--close').addEventListener('click', hideLightbox);

      lightboxNextButton.addEventListener('click', showNextSlide);
      lightboxPrevButton.addEventListener('click', showPrevSlide);

      const hammer = new Hammer(lightbox);
      hammer.on('swipeleft', showNextSlide);
      hammer.on('swiperight', showPrevSlide);

      setTimeout(() => {
        new LazyLoad({
          elements_selector: '.lazy',
          threshold: 100,
        });
      }, 0);
    }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
