{{--
  Template Name: Shops Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="blocks">
    <div class="block">
      <div class="block__text">
        @php $rows = get_field('shops') @endphp
        @if ($rows)
        <div class="inner">
          @include('partials.content-page')
          <img class="stroke" src="@asset('images/stroke.png')" />
          <div class="shops">
            @foreach ($rows as $row)
            <div class="shop">
              @php echo html_entity_decode($row['shop']) @endphp
            </div>
            @endforeach
          </div>
        </div>
        @endif
      </div>
      <div class="block__image" style="background-image: url({{ the_field('shops_background') }})"></div>
    </div>
  </div>
  @endwhile
@endsection
