<div class="lightbox-overlay"></div>
<div class="lightbox" style="display: none;">
    <div class="container">
        <img class="lightbox__button lightbox__button--close" src="@asset('images/lightbox-close.png')" />
        <img class="lightbox__button lightbox__button--prev" src="@asset('images/lightbox-button.png')" />
        <img class="lightbox__image" src="" />
        <img class="lightbox__button lightbox__button--next" src="@asset('images/lightbox-button.png')" />
    </div>
</div>