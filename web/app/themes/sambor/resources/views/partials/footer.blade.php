<footer class="content-info">
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp
    2018 Piekarnia Sambor. Wszystkie prawa zastrzeżone

    <a class="facebook-icon" href=""><img src="@asset('images/facebook-logo.png')" /></a>
  </div>
</footer>
