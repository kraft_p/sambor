{{--
  Template Name: Products Template
--}}

@extends('layouts.app')

@section('content')
    @while (have_posts()) @php the_post() @endphp

    @if (have_rows('products_list'))
    <div class="grid">
        <div class="grid__item grid__item--category">
            <img class="thumbnail lazy" data-src="{{ get_field('products_category_image')['sizes']['product-medium'] }}" />
        </div>
        @php $loopIteration = 0; $productsCount = count(get_field('products_list')); @endphp
        @while(have_rows('products_list')) @php the_row() @endphp
        @php $loopIteration++ @endphp
        <div class="grid__item grid__item--{{ $loopIteration }}">
            <img class="thumbnail lazy" data-src="{{ get_sub_field('product_image')['sizes']['product-medium'] }}" data-lightboxsrc="{{ get_sub_field('product_image')['sizes']['product-large'] }}" />
            <div class="title">
                <span>{{ get_sub_field('product_title') }}</span>
            </div>
            @if ($loopIteration == 6)
            <img class="scroll-down" src="@asset('images/scroll-button.png')" />
            @elseif ($loopIteration > 6 && $loopIteration < ($productsCount - 4) && $loopIteration % 8 == 6)
            <img class="scroll-down scroll-down--narrow-site" src="@asset('images/scroll-button.png')" />
            @elseif ($loopIteration > 7 && $loopIteration < ($productsCount - 4) && $loopIteration % 10 == 7)
            <img class="scroll-down scroll-down--wide-site" src="@asset('images/scroll-button.png')" />
            @endif
        </div>
        @endwhile

        @while(have_rows('plugs_list')) @php the_row() @endphp
        <div class="grid__item grid__item--empty">
            <img class="thumbnail lazy" data-src="{{ get_sub_field('plug_image')['sizes']['product-medium'] }}" />
        </div>
        @endwhile
    </div>
    @endif
  @endwhile
@endsection
