{{--
  Template Name: About Template
--}}

@extends('layouts.app')

@section('content')
    @while (have_posts()) @php the_post() @endphp

    <div class="header-background" style="background-image: url({{ the_field('header_background') }})">
        <img data-scrollTo=".blocks" class="scroll-down" src="@asset('images/scroll-button-white.png')" />
    </div>

    @if (have_rows('blocks'))
    <div class="blocks">
        @php $rows = get_field('blocks') @endphp

        @foreach ($rows as $row)
        <div class="block block--{{ $loop->iteration }}">
            <div class="block__text">
                <div class="container">
                    @if ($row['block_title'])
                    <h2 class="block__title">@php echo $row['block_title'] @endphp</h2>
                    @endif
                    @php echo $row['block_text'] @endphp

                    @if ($loop->iteration % 2 == 0)
                    <img class="stroke" src="@asset('images/stroke.png')" />
                    @endif
                </div>
            </div>
            <div class="block__image" style="background-image: url({{ $row['block_image'] }})"></div>
            
            @if ($loop->last)
                <img class="scroll-up" data-scrollTo=".header-background" src="@asset('images/scroll-button.png')" />
            @else
                <img class="scroll-down" data-scrollTo=".block--{{ $loop->iteration + 1 }}" src="@asset('images/scroll-button.png')" />
            @endif
        </div>
        @endforeach
    </div>
    @endif
  @endwhile
@endsection
