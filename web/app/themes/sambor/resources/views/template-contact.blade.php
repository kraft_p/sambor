{{--
  Template Name: Contact Template
--}}

@extends('layouts.app')

@section('content')
<div class="blocks">
  <div class="block">
    <div class="block__text">
      @php $rows = get_field('contact_details') @endphp
      @if ($rows)
      <div class="contact-details">
        @foreach ($rows as $row)
        <div class="contact-detail">
          @php echo html_entity_decode($row['adress']) @endphp
        </div>
        @if ($loop->iteration % 2 && !$loop->last)
        <img class="stroke" src="@asset('images/stroke.png')" />
        @endif
        @endforeach
      </div>
      @endif
    </div>
    <div class="block__image" style="background-image: url({{ the_field('contact_background') }})"></div>
  </div>
</div>
@endsection
